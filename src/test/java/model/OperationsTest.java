package model;

import javafx.util.Pair;
import org.junit.*;

import static org.junit.Assert.*;

public class OperationsTest {
    private Polynomial polinom1;
    private Polynomial polinom2;
    private Polynomial polinom3;
    private Polynomial polinom4;
    private Polynomial polinom5;
    private Polynomial rezultat;
    @BeforeClass
    public static void start(){
        System.out.println("Acest mesaj va fi afisat o singura data la initializarea testelor.\n Aceasta clasa testeaza toate operatiile cu polinoame cerute.\n");


    }

    @Before
    public void setUp() throws Exception {
        polinom1 = new Polynomial();
        polinom2 = new Polynomial();
        polinom3 = new Polynomial();
        polinom4 = new Polynomial();
        polinom5 = new Polynomial();
        rezultat = new Polynomial();
        polinom1.readPolynom("2x^2-2");
        polinom2.readPolynom("2x^2+2");
        polinom3.readPolynom("2x^1+2");
        polinom4.readPolynom("3x^3-x^2-2");
        polinom5.readPolynom("x^2+2");
    }

    @After
    public void tearDown() throws Exception {
        polinom1.resetPolynom();
        polinom2.resetPolynom();
        polinom3.resetPolynom();
        polinom4.resetPolynom();
        rezultat.resetPolynom();
    }

    @Test
    public void multiplication() {
       rezultat.readPolynom("4x^4-4");
       assertTrue("Rezultatul inmultire",Operations.multiplication(polinom1,polinom2).toString().equals(rezultat.toString()));
    }

    @Test
    public void addition() {
        rezultat.readPolynom("4x^2");
        assertTrue("Rezultat adunare",Operations.addition(polinom1,polinom2).toString().equals(rezultat.toString()));
    }

    @Test
    public void derivate() {
        rezultat.readPolynom("4x^1");
        Operations.derivate(polinom1);
        assertTrue("Rezultat derivare",polinom1.toString().equals(rezultat.toString()));
    }

    @Test
    public void integrate() {
       String rezultat = "+X^2 + 2*X^1";
        Operations.integrate(polinom3);

        assertTrue("Rezultat integrare",polinom3.toString().equals(rezultat.toString()));
    }

    @Test
    public void division() {
        Pair<Polynomial,Polynomial> rez = Operations.division(polinom4,polinom5);
        rezultat.readPolynom("+3x -1");
        Polynomial cat = new Polynomial();
        cat.readPolynom("-6x");
        assertTrue("Rezultat division",rez.getKey().toString().equals(rezultat.toString())&& rez.getValue().toString().equals(cat.toString()));

    }

    @Test
    public void substraction() {
        rezultat.readPolynom("-4");
        assertTrue("Rezultat substraction",Operations.substraction(polinom1,polinom2).toString().equals(rezultat.toString()));
    }




}