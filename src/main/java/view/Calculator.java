package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import java.awt.SystemColor;

public class Calculator extends JFrame {

    private JPanel contentPane;
    private JTextField textResulted;
    private JTextField textRemainder;
    private JLabel titleLabel;
    private JLabel polinomLabel1;
    private JTextField textPolinom1;
    private JLabel polinomLabel2;
    private JTextField textPolinom2;
    private JLabel operationSignLabel;
    private JSeparator separator;
    private JSeparator separator_1;
    private JLabel resultLabel;
    private JLabel remainderLabel;
    private JSeparator separator_2;
    private JButton additionButton;
    private JButton multipliButton;
    private JButton divisionButton;
    private JButton derivateButton;
    private JButton integrateButton;
    private JLabel typeLabel;
    private JLabel polynom1Label;
    private JLabel polynom2Label;
    private JLabel imageLabel;
    private JLabel authorLabel;
    private JButton helpButton;
    private JScrollPane scrollPane;
    private JTextPane textReguli;
    private JButton substractionButton;


    /**
     * Launch the application.
     */

    /**
     * Create the frame.
     */
    public Calculator() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 716, 449);
        contentPane = new JPanel();
        contentPane.setBackground(SystemColor.menu);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        titleLabel = new JLabel("Calculator Polinoame");
        titleLabel.setBounds(198, 11, 255, 24);
        titleLabel.setFont(new Font("Cascadia Code", Font.PLAIN, 20));
        contentPane.add(titleLabel);

        polinomLabel1 = new JLabel("First Polynom  = ");
        polinomLabel1.setBounds(10, 109, 128, 24);
        polinomLabel1.setFont(new Font("Cascadia Mono", Font.BOLD, 12));
        contentPane.add(polinomLabel1);

        textPolinom1 = new JTextField();
        textPolinom1.setBounds(163, 109, 267, 24);
        textPolinom1.setFont(new Font("Cascadia Code", Font.PLAIN, 13));
        textPolinom1.setText("3x^3-x^2-2");
        contentPane.add(textPolinom1);

        polinomLabel2 = new JLabel("Secound Polynom = ");
        polinomLabel2.setBounds(10, 171, 133, 14);
        polinomLabel2.setFont(new Font("Cascadia Code", Font.BOLD, 12));
        contentPane.add(polinomLabel2);

        textPolinom2 = new JTextField();
        textPolinom2.setBounds(163, 171, 267, 24);
        textPolinom2.setFont(new Font("Cascadia Code", Font.PLAIN, 13));
        textPolinom2.setText("x^2+2");
        contentPane.add(textPolinom2);

        operationSignLabel = new JLabel("+");
        operationSignLabel.setBounds(277, 146, 29, 14);
        operationSignLabel.setVisible(false);
        operationSignLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        contentPane.add(operationSignLabel);

        separator = new JSeparator();
        separator.setBounds(10, 206, 680, 4);
        contentPane.add(separator);

        separator_1 = new JSeparator();
        separator_1.setBounds(10, 90, 680, 4);
        contentPane.add(separator_1);

        resultLabel = new JLabel("Polynom Resulted =");
        resultLabel.setBounds(10, 227, 133, 14);
        resultLabel.setFont(new Font("Cascadia Code", Font.BOLD, 12));
        contentPane.add(resultLabel);

        textResulted = new JTextField();
        textResulted.setBounds(163, 222, 267, 24);
        textResulted.setFont(new Font("Cascadia Code", Font.PLAIN, 13));
        contentPane.add(textResulted);
        textResulted.setColumns(10);

        remainderLabel = new JLabel("Remainder Polynom = ");
        remainderLabel.setBounds(10, 260, 151, 14);
        remainderLabel.setVisible(false);
        remainderLabel.setFont(new Font("Cascadia Code", Font.BOLD, 12));
        contentPane.add(remainderLabel);

        textRemainder = new JTextField();
        textRemainder.setBounds(163, 256, 267, 24);
        textRemainder.setFont(new Font("Cascadia Code", Font.PLAIN, 13));
        textRemainder.setVisible(false);
        contentPane.add(textRemainder);
        textRemainder.setColumns(10);

        separator_2 = new JSeparator();
        separator_2.setBounds(10, 285, 680, 6);
        contentPane.add(separator_2);

        additionButton = new JButton("Addition(+)");
        additionButton.setBounds(10, 302, 128, 46);
        additionButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        contentPane.add(additionButton);

        multipliButton = new JButton("Multiplication ( * )");
        multipliButton.setBounds(10, 359, 128, 46);
        multipliButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        contentPane.add(multipliButton);

        divisionButton = new JButton("Divizion ( / )");
        divisionButton.setBounds(148, 302, 115, 46);
        divisionButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        contentPane.add(divisionButton);

        derivateButton = new JButton("Derivation ( ' )");
        derivateButton.setBounds(148, 359, 115, 46);
        derivateButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        contentPane.add(derivateButton);

        integrateButton = new JButton("Integration ( S )");
        integrateButton.setBounds(273, 359, 115, 46);
        integrateButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        contentPane.add(integrateButton);

        typeLabel = new JLabel("Derivated(')");
        typeLabel.setBounds(500, 72, 106, 14);
        typeLabel.setVisible(false);
        typeLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
        contentPane.add(typeLabel);

        polynom1Label = new JLabel("Poly1 derivated");
        polynom1Label.setBounds(444, 114, 246, 14);
        polynom1Label.setVisible(false);
        polynom1Label.setFont(new Font("Cascadia Code", Font.BOLD, 13));
        contentPane.add(polynom1Label);

        polynom2Label = new JLabel("Poly2 Derivated");
        polynom2Label.setBounds(444, 171, 246, 14);
        polynom2Label.setVisible(false);
        polynom2Label.setFont(new Font("Cascadia Code", Font.BOLD, 13));
        contentPane.add(polynom2Label);

        imageLabel = new JLabel("Image");
        imageLabel.setBounds(10, 3, 133, 46);
        imageLabel.setIcon(new ImageIcon("D:\\Facultate\\Tehnici PROGRAMARE\\PT2022_30229_Zaharie_Andrei_Assignment_1\\src\\main\\resources\\Image_10.png"));
        contentPane.add(imageLabel);

        authorLabel = new JLabel("Made by Zaharie Andrei");
        authorLabel.setBounds(10, 46, 185, 33);
        authorLabel.setFont(new Font("Cascadia Code", Font.ITALIC, 12));
        contentPane.add(authorLabel);

        helpButton = new JButton("Ascunde Reguli");
        helpButton.setBounds(462, 241, 207, 33);
        helpButton.setFont(new Font("Cascadia Code", Font.BOLD, 14));
        contentPane.add(helpButton);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(423, 293, 267, 112);
        contentPane.add(scrollPane);

        textReguli = new JTextPane();
        textReguli.setText(" Model de introducere a polinoamelor:\r\n  1. Coeficientii trebuie pusi in fata literei (Se poate folosi litera x sau X).\r\n  2. Intre coeficienti si litera nu trebuie pus semnul de inmultire (*).\r\n  3. Exponenti trebuie scrisi dupa necunoscuta folosind semnul '^'.\r\n  4. OBSERVATIE: Orice alt mod de scriere va duce la o functionare defectuoasa!\r\nEXEMPLU: 5x^2-3x+ 90 -7x^-1 ...");
        scrollPane.setViewportView(textReguli);

        substractionButton = new JButton("Substraction ( - )");
        substractionButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
        substractionButton.setBounds(273, 302, 115, 46);
        contentPane.add(substractionButton);
    }
    public void resetOptions(){
        polynom1Label.setVisible(false);
        polynom2Label.setVisible(false);
        typeLabel.setVisible(false);
        textRemainder.setVisible(false);
        remainderLabel.setVisible(false);
        operationSignLabel.setVisible(false);
        textResulted.setText("");
    }
    public void setHelpButton(String mesaj){
        helpButton.setText(mesaj);

    }
    public void setScrollPane(boolean visible){
        scrollPane.setVisible(visible);
    }
    public boolean getScrollPaneVisibility(){
        return scrollPane.isVisible();
    }
    public String getPolynom1(){
        return textPolinom1.getText();
    }
    public String getPolynom2(){
        return textPolinom2.getText();
    }

    public void setTypeVisibility(boolean visible,String type){
        typeLabel.setText(type);
        typeLabel.setVisible(visible);
    }
    public void setPolynom1LabelVisibility(boolean visible,String polinom1,String polinom2){
        polynom1Label.setText(polinom1);
        polynom1Label.setVisible(visible);

        polynom2Label.setText(polinom2);
        polynom2Label.setVisible(visible);
    }

    public void setOperationSignLabel(boolean visible,String operationSign){
        operationSignLabel.setText(operationSign);
        operationSignLabel.setVisible(visible);
    }
    public void setRemainder(boolean visible,String remainder)
    {
        remainderLabel.setVisible(visible);
        textRemainder.setVisible(visible);
        textRemainder.setText(remainder);

    }
    public void setResultedPolynom(String polynom){
        textResulted.setText(polynom);
    }
    public void showErrorMessage(String mesaj) {
        JOptionPane.showMessageDialog(this, mesaj);
    }

    public void additionButtonListener(ActionListener actionListener){
        this.additionButton.addActionListener(actionListener);
    }
    public void substractionButtonListener(ActionListener actionListener){
        this.substractionButton.addActionListener(actionListener);
    }
    public void divisionButtonListener(ActionListener actionListener){
        this.divisionButton.addActionListener(actionListener);
    }
    public void multipliButtonListener(ActionListener actionListener){
        this.multipliButton.addActionListener(actionListener);
    }
    public void derivateButtonListener(ActionListener actionListener){
        this.derivateButton.addActionListener(actionListener);
    }
    public void intgrateButtonListener(ActionListener actionListener){
        this.integrateButton.addActionListener(actionListener);
    }
    public void helpButtonListener(ActionListener actionListener){
        this.helpButton.addActionListener(actionListener);
    }
}
