package model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynomial {
    private List<Monomial> monomialList;
    private int grade;

    public Polynomial(){
        monomialList = new LinkedList<Monomial>();

    }

    public List<Monomial> getMonomialList() {
        return monomialList;
    }

    public void setMonomialList(List<Monomial> monomialList) {
        this.monomialList = monomialList;
    }




    public void readPolynom(String polynom) {
        polynom.replaceAll("\\s+",""); // scap de spatiile nedorite
        //Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Pattern p = Pattern.compile("([-+]?)(\\d*\\.?\\d*)?([xX](\\^-?\\d*\\.?\\d*)?)?");
        Matcher m = p.matcher(polynom);
        while (m.find()) {
            /*System.out.println("sign: " + m.group(1));
            System.out.println("coef: " + m.group(2));
            System.out.println("g2: " + m.group(3));
            System.out.println("nr: " + m.group(4));*/
            String coef =m.group(1) + m.group(2);
            if(m.group(2).isEmpty() && m.group(4) == null)
                coef = "0";
            else if(m.group(2).isEmpty() && m.group(4) != null)
                coef  += "1";
            String exp = "0";
            if(m.group(4) != null)
               exp = m.group(4).substring(1);
            else if(m.group(4) == null && m.group(3)!=null)
                exp = "1";
            //System.out.println("Coef: " + Integer.parseInt(coef) + " exp: " + Integer.parseInt(exp));
            Monomial monom = new Monomial(Integer.parseInt(coef),Integer.parseInt(exp));
            monomialList.add(monom);
        }
        Monomial.simplification(monomialList);

    }
    public void reorderPolynom(){
        Collections.sort(this.getMonomialList());
    }

    public String toString(){
        String polinom="";
        for(Monomial list: monomialList){
            if(list.getCoef() > 0)
                polinom+= "+" + list.toString();
            else if(list.getCoef() < 0)
                polinom+= list.toString();
        }
        return polinom;
    }
   public void addToPolynom(Monomial monom){
        this.monomialList.add(monom);
        this.reorderPolynom();
   }
   public void resetPolynom(){
        this.monomialList.clear();
   }

   public int getFirstCoef(){
        return this.monomialList.get(0).getCoef();
   }
   public int getFirstExpon(){
        return this.monomialList.get(0).getExponent();
   }



}
