package model;

import java.util.List;

public class Monomial implements Comparable<Monomial>{
    private int exponent;
    private int coef;
    private boolean parcurs;
    private int coefInegrare;

    public int getExponent() {
        return exponent;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    public int getCoef() {
        return coef;
    }

    public void setCoef(int coef) {
        this.coef = coef;
    }

    public Monomial(int coef, int exponent){
        this.exponent = exponent;
        this.coef = coef;
        this.coefInegrare = 0;
        this.parcurs = false;
    }
    public static void simplification(List<Monomial> monomList){
        for(Monomial first: monomList){
            for(Monomial secound:monomList){
                if(first != secound){
                if(first.exponent == secound.exponent && first.coef != 0 && secound.coef !=0){
                    //System.out.println(first.exponent + " " + secound.exponent); // Folosit pt debugging
                    //System.out.println(first.coef + " " + secound.coef);
                    first.coef += secound.coef;
                    secound.coef = 0;

                }
                }
            }
        }
        monomList.removeIf(delete -> delete.coef == 0);
    }

    public String toString(){
        if(this.coefInegrare == 0) {
            if (this.coef == 1 && (this.exponent > 1 || this.exponent < 0))
                return " X^" + this.exponent + " ";
            else if (this.coef == 1 && this.exponent == 1)
                return " X ";
            if (this.exponent == 1 && this.coef != 0)
                return this.coef + "X";
            if (this.exponent == 0)
                return " " + this.coef;
            else
                return " " + this.coef + "*X^" + this.exponent;
        }
        else {
            int rest = this.coef % this.coefInegrare;
            if(this.coefInegrare != 1 && rest !=0)
            return " " + this.coef + "/" + this.coefInegrare + "*X^" + this.exponent;
            else {
                int rez = this.coef / this.coefInegrare;
                if(rez != 1)
                return " " + rez + "*X^" + this.exponent;
                else return "X^" + this.exponent + " " ;
            }
        }
    }

    public boolean isParcurs() {
        return parcurs;
    }

    public void setParcurs(boolean parcurs) {
        this.parcurs = parcurs;
    }

    @Override
    public int compareTo(Monomial o) {
        return o.exponent - this.exponent;
    }

    public int getCoefInegrare() {
        return coefInegrare;
    }
    public void setCoefInegrare(int coefInegrare) {
        this.coefInegrare = coefInegrare;
    }


}
