package model;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.List;

public class  Operations {
    public static Polynomial multiplication(Polynomial polinom1, Polynomial polinom2){
        Polynomial polinomRezultat = new Polynomial();
        System.out.print("("+polinom1 + ") * (" + polinom2+") =");
        int coef , exponent;
        for(Monomial first : polinom1.getMonomialList()){
            for(Monomial secound : polinom2.getMonomialList()){
                coef = first.getCoef() * secound.getCoef();
                exponent = first.getExponent() + secound.getExponent();
                Monomial rez = new Monomial(coef,exponent);
                polinomRezultat.getMonomialList().add(rez);
               // System.out.println(coef + "x^" + exponent); // Pentru debugging
            }
        }
        Monomial.simplification(polinomRezultat.getMonomialList());
        polinomRezultat.reorderPolynom();
        return polinomRezultat;
    }

    public static Polynomial addition(Polynomial polinom1, Polynomial polinom2){

        Polynomial rezultat = new Polynomial();
        int coef;

        Monomial.simplification(polinom1.getMonomialList());
        Monomial.simplification(polinom2.getMonomialList());
        System.out.print("("+polinom1 + ") + (" + polinom2+") =");
        for(Monomial first : polinom1.getMonomialList()){
            for(Monomial secound : polinom2.getMonomialList()){
               if(first.getExponent() == secound.getExponent()){
                   first.setParcurs(true);
                   secound.setParcurs(true);

                   coef = first.getCoef() + secound.getCoef();

                   Monomial rez = new Monomial(coef,first.getExponent());
                   rezultat.getMonomialList().add(rez);
               }

            }

        }
        //daca a mai ramas un termen din primul polinom care nu a fost adaugat
        for(Monomial poli1 : polinom1.getMonomialList()){
            if(!poli1.isParcurs()){
                Monomial rez = new Monomial(poli1.getCoef(),poli1.getExponent());
                rezultat.getMonomialList().add(rez);
            }
        }
        //daca a mai ramas un termen din al doilea polinom care nu a fost adaugat
        for(Monomial poli2 : polinom2.getMonomialList()){
            if(!poli2.isParcurs()){
                Monomial rez = new Monomial(poli2.getCoef(),poli2.getExponent());
                rezultat.getMonomialList().add(rez);
            }
        }

        rezultat.reorderPolynom();

       return rezultat;
    }
    public static Polynomial substraction(Polynomial polinom1, Polynomial polinom2){
    Polynomial rezultat = new Polynomial();
    Operations.scalarMultiply(polinom2,-1);
        System.out.println(polinom2);
    rezultat = Operations.addition(polinom1,polinom2);
    return rezultat;
}
    public static void derivate(Polynomial polinom){
        System.out.print("("+polinom + ")' =");
        for(Monomial poli: polinom.getMonomialList()){
            if(poli.getExponent() == 0){
                poli.setCoef(poli.getCoef() * poli.getExponent());
            }
            else{
                poli.setCoef(poli.getCoef() * poli.getExponent());
                poli.setExponent(poli.getExponent() - 1);
            }
        }
        polinom.reorderPolynom();
        System.out.println(polinom);
    }

    public static void integrate(Polynomial polinom){
        //System.out.print("S("+polinom + ") =");
        for(Monomial poli: polinom.getMonomialList()){
            if(poli.getExponent() != -1){
                if(poli.getCoef() < 0){
                    poli.setCoef(poli.getCoef() * -1);
                    poli.setCoefInegrare((poli.getExponent() + 1)* -1);
                    poli.setExponent(poli.getExponent() + 1);
                }
                else{
                    poli.setCoefInegrare(poli.getExponent() + 1);
                    poli.setExponent(poli.getExponent() + 1);
                }

            }
            else poli.setCoef(0);
        }
        //System.out.println(polinom);
    }

    public static void scalarMultiply(Polynomial polinom, int x){
        for(Monomial first :polinom.getMonomialList()){
            first.setCoef(first.getCoef() * x);
        }
    }

    public static Pair<Polynomial,Polynomial> division(Polynomial polinom1, Polynomial polinom2){
        Polynomial cat = new Polynomial();
        Polynomial rest = new Polynomial();
        Polynomial imparitor = new Polynomial();
        polinom1.reorderPolynom();
        polinom2.reorderPolynom();

       if(polinom1.getMonomialList().get(0).getExponent() > polinom2.getMonomialList().get(0).getExponent()){
           rest = polinom1;
           imparitor = polinom2;

           while(rest.getFirstExpon()>= imparitor.getFirstExpon()){
               int newCoef = rest.getFirstCoef() / imparitor.getFirstCoef();
               int newExpon = rest.getFirstExpon() - imparitor.getFirstExpon();
               Monomial monom = new Monomial(newCoef,newExpon);
               cat.addToPolynom(monom); //adaug elementele la cat

               Polynomial aux = new Polynomial();
               aux.addToPolynom(monom); // imi creez elementul din cat pe care urmeaza sa il inmultesc cu imartitorul
               System.out.println(aux);
               aux = Operations.multiplication(aux,imparitor);
               Operations.scalarMultiply(aux,-1);
               System.out.println(aux);
               rest = Operations.addition(rest,aux);
               System.out.println(rest);
               aux.getMonomialList().clear(); //golesc polynomul aux pe care o sa il refolosesc pentru urmatorul monom descoperit

           }

       }
       else{
           Polynomial zero = new Polynomial();
           zero.readPolynom("0x^1");
           cat = zero;
           rest = polinom1;
       }

        //System.out.println("Cat = " + cat + "\n rest = " + rest);

        return new Pair<Polynomial,Polynomial>(cat,rest);

    }
}
