package controller;

import javafx.util.Pair;
import model.Operations;
import model.Polynomial;
import view.Calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

public class Controller {
    private Calculator calc;
    private Polynomial poli1;
    private Polynomial poli2;
    private boolean check(String polinom1, String polinom2){
        polinom1.toLowerCase();
        polinom2.toLowerCase();
        if(polinom1.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*") || polinom2.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*")){
            String mesaj = "";
            if(polinom1.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*") && polinom2.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*") ){
                mesaj += "Ambele polinoame au fost introduse gresit!";
            }
            else if(polinom2.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*")){
                mesaj += "Al doilea polinom a fost introdus gresit!";
            }
            else if(polinom1.matches(".*[abcdefghijklnmpqrostuvwyz;:',><!@#$%&()_=].*")){
                mesaj += "Primul polinom a fost introdus gresit!";
            }
            calc.showErrorMessage(mesaj);
            return false;
        }
        return true;
    }
    public Controller(Calculator calculator,Polynomial polinom1, Polynomial polinom2){
    this.calc = calculator;
    this.poli1 = polinom1;
    this.poli2 = polinom2;
    calculator.additionButtonListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1=calculator.getPolynom1();
            String polynom2=calculator.getPolynom2();
            if(check(polynom1,polynom2)) {
                polinom1.readPolynom(polynom1);
                polinom2.readPolynom(polynom2);
                calculator.setOperationSignLabel(true, "+");
                Polynomial rezultat = Operations.addition(polinom1, polinom2);
                calculator.setResultedPolynom(rezultat.toString());

                // Dau reset la polinoame pentru a rezolva cazul in care utilizatorul apasa de mai multe ori pe aceeasi operatie
                // iar la polinomul deja citit se aduna iar acelasi polinom.
                polinom1.resetPolynom();
                polinom2.resetPolynom();
                rezultat.resetPolynom();
            }
        }
    });

    calculator.substractionButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1 = calculator.getPolynom1();
            String polynom2 = calculator.getPolynom2();
            if (check(polynom1, polynom2)) {
                polinom1.readPolynom(polynom1);
                polinom2.readPolynom(polynom2);
                calculator.setOperationSignLabel(true, "-");
                Polynomial rezultat = Operations.substraction(polinom1, polinom2);
                calculator.setResultedPolynom(rezultat.toString());

                // Dau reset la polinoame pentru a rezolva cazul in care utilizatorul apasa de mai multe ori pe aceeasi operatie
                // iar la polinomul deja citit se aduna iar acelasi polinom.
                polinom1.resetPolynom();
                polinom2.resetPolynom();
                rezultat.resetPolynom();
            }
        }
    });

    calculator.divisionButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1 = calculator.getPolynom1();
            String polynom2 = calculator.getPolynom2();
            if (check(polynom1, polynom2)) {

                polinom1.readPolynom(polynom1);
                polinom2.readPolynom(polynom2);
                calculator.setOperationSignLabel(true, "/");

                Pair<Polynomial, Polynomial> rezultat = Operations.division(polinom1, polinom2);
                if (rezultat.getKey().getMonomialList().isEmpty())
                    calculator.setResultedPolynom("0");
                else {
                    calculator.setResultedPolynom(rezultat.getKey().toString());
                }
                calculator.setRemainder(true, rezultat.getValue().toString());

                polinom1.resetPolynom();
                polinom2.resetPolynom();
                rezultat.getKey().resetPolynom();
                rezultat.getValue().resetPolynom();

            }
        }
    });

    calculator.multipliButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1=calculator.getPolynom1();
            String polynom2=calculator.getPolynom2();
            if (check(polynom1, polynom2)){

            polinom1.readPolynom(polynom1);
            polinom2.readPolynom(polynom2);
            calculator.setOperationSignLabel(true,"X");
            Polynomial rezultat = Operations.multiplication(polinom1,polinom2);
            calculator.setResultedPolynom(rezultat.toString());

            polinom1.resetPolynom();
            polinom2.resetPolynom();
            rezultat.resetPolynom();

        }}

    });

    calculator.derivateButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1 = calculator.getPolynom1();
            String polynom2 = calculator.getPolynom2();
            if (check(polynom1, polynom2)) {

                polinom1.readPolynom(polynom1);
                polinom2.readPolynom(polynom2);
                calculator.setTypeVisibility(true, "(Derivated)'");

                Operations.derivate(polinom1);
                Operations.derivate(polinom2);

                calculator.setPolynom1LabelVisibility(true, polinom1.toString(), polinom2.toString());

                polinom1.resetPolynom();
                polinom2.resetPolynom();
            }
        }
    });

    calculator.intgrateButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calculator.resetOptions();
            String polynom1 = calculator.getPolynom1();
            String polynom2 = calculator.getPolynom2();
            if (check(polynom1, polynom2)) {

                polinom1.readPolynom(polynom1);
                polinom2.readPolynom(polynom2);
                calculator.setTypeVisibility(true, "S(Integrated)");

                Operations.integrate(polinom1);
                Operations.integrate(polinom2);

                calculator.setPolynom1LabelVisibility(true, polinom1.toString(), polinom2.toString());

                polinom1.resetPolynom();
                polinom2.resetPolynom();
            }
        }
    });

    calculator.helpButtonListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(calculator.getScrollPaneVisibility()){
                calculator.setScrollPane(false);
                calculator.setHelpButton("Reguli de scriere");
            }
            else
            {
                calculator.setScrollPane(true);
                calculator.setHelpButton("Ascunde Reguli");
            }
        }
    });

    }
}
